---
--
--
--
---

function onInit()
	-- This can be used where we set flags on tokens in the CT
	-- that are not "in" the combat tracker. Allowing you to pre-load
	-- all the NPCs on the map and then only "show" those visible.
	-- Celestian
	-- replace default CombatManager.getCombatantNodes()
	CombatManager.setCustomGetCombatantNodes(getCombatantNodes_sa);

	-- this lets us type "/saclean all" and remove any hidden entries we can't find.
	if User.isHost() then
		Comm.registerSlashHandler("saclean", processTokenCleaning);
	end

end

function onClose()
end


--[[
	Remove ALL children in the combattracker, hidden or otherwise
]]
function processTokenCleaning(sCommand, sParams)
	if sParams == "all" then
		DB.deleteChildren('combattracker.list');
		ChatManager.SystemMessage("Deleted ALL combattracker entries.");
	else
		ChatManager.SystemMessage("options: saclean [all]\nRemoves ALL CT entries, hidden or otherwise entirely.");
	end
end

--[[
  Get a list of combatants from the CT that are supposed to be visible in the CT
  (and don't get the ones not flagged as in the CT)
]]
function getCombatantNodes_sa()
--Debug.console("manager_sa","getCombatantNodes_sa");
  local aCTUnfilteredEntries = DB.getChildren("combattracker.list");
  local aCTFilteredEntries = {};

  for _,node in pairs(aCTUnfilteredEntries) do
    local bCTVisible = (DB.getValue(node,"ct.visible",1) == 1);
    if bCTVisible then
      table.insert(aCTFilteredEntries,node);
    end
  end

  return aCTFilteredEntries;
end
  
--[[
	At some point we need to get ALL the tokens, not just the tokens
	marked as active. This function will get unfiltered list of all the 
	tokens in the same imageControl as this token
]]--
function getCTFromTokenUnfiltered(token)
	if not token then
		return nil;
	end
	
	local nodeContainer = token.getContainerNode();
	local nID = token.getId();

	return getCTFromTokenRefUnfiltered(nodeContainer, nID);
end
function getCTFromTokenRefUnfiltered(vContainer, nId)
	local sContainerNode = nil;
	if type(vContainer) == "string" then
		sContainerNode = vContainer;
	elseif type(vContainer) == "databasenode" then
		sContainerNode = vContainer.getNodeName();
	else
		return nil;
	end
	
	for _,v in pairs(DB.getChildren("combattracker.list")) do
		local sCTContainerName = DB.getValue(v, "tokenrefnode", "");
		local nCTId = tonumber(DB.getValue(v, "tokenrefid", "")) or 0;
		if (sCTContainerName == sContainerNode) and (nCTId == nId) then
			return v;
		end
	end
	
	return nil;
end

--[[
  activate/deActivateSelectedTokensInCT selected tokens in current imageControl 
]]
function activateSelectedTokensInCT(imageControl)
	manageActivationForSelectedTokens(imageControl,1);
end
function deActivateSelectedTokensInCT(imageControl)
	manageActivationForSelectedTokens(imageControl,0);
end
function manageActivationForSelectedTokens(imageControl,nActiveState)
	if imageControl then
		local aSelected = imageControl.getSelectedTokens();
		for _,nodeToken in pairs(aSelected) do
		  local nodeCT = getCTFromTokenUnfiltered(nodeToken);
		  if nodeCT then
			DB.setValue(nodeCT,"ct.visible","number",nActiveState);
			if nActiveState ~= 1 then
				DB.setValue(nodeCT,"tokenvis","number",0);
			else
				-- toggle effects back on since we're moving it back
				-- to active state
				TokenManager.updateEffectsHelper(nodeToken, nodeCT);				
			end
		  end
		end
	end
end
